# Changelog

- This package is now pure ESM. Please [read this](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c).

## 4.5.0 (2023-01-02)

### Features

- add exhaustive guard function

## 4.4.0 (2022-04-10)

### Features

- add AssertionError class
- isSet and assertSet now return NonNullable type

## 4.3.1 (2022-04-05)

## 4.3.0 (2022-02-13)

## 4.2.0 (2021-12-24)

### Features

- add TS Modify type as wrapper for Omit
- add isSet and assertSet functions

## 4.1.0 (2021-12-15)

## 4.0.1 (2021-11-09)

## 4.0.0 (2021-11-09)

## 3.0.0 (2021-10-04)

### Features

- change to ESM

## 2.0.1 (2021-10-04)

### Miscellaneous Chores

- prettier should not format tsconfig
- update commitlint monorepo to v13.2.0
- update dependency @4s1/changelog-config to v2

## 2.0.0 (2021-09-21)

### ⚠ BREAKING CHANGES

- FourS1Error -> DefaultError

### Features

- add bootstrap 3+5 colors
- add console colors
- add logger
- rename error class

### Bug Fixes

- simplify color definitions

### Continuous Integration

- add cache setting
- bring .gitlab-ci.yml to standard format
- remove semantic-release stuff
- remove unused var

### Documentation

- fix changelog

### Miscellaneous Chores

- project settings
- typo
- update dependency @types/node to v14.17.17
- update dependency prettier to v2.4.1

## 1.2.3 (2021-09-15)

### Features

- use standard-version

### Bug Fixes

- unused packages removed

### Miscellaneous Chores

- update dependency @semantic-release/git to v9.0.1
- update dependency @types/node to v14.17.16

## 1.2.2 (2021-09-11)

### Bug Fixes

- darkWhite color

## 1.2.1 (2021-09-11)

### Bug Fixes

- all colors will be published

### Renovate Bot

- update dependency @4s1/eslint-config to v3.1.2

## 1.2.0 (2021-09-11)

### Features

- add custom error class

## 1.1.0 (2021-09-11)

### Features

- Add colors

### Renovate Bot

- update dependency typescript to v4.4.3

## 1.0.0 (2021-09-11)
