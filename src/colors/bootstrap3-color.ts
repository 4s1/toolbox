/**
 * Bootstrap3 colors
 *
 * Based on https://getbootstrap.com/docs/3.4/customize/#colors
 */
export abstract class Bootstrap3Color {
  /**
   * called "Primary"
   */
  static readonly blue = "#337ab7";

  /**
   * called "Success"
   */
  static readonly green = "#5cb85c";

  /**
   * called "Info"
   */
  static readonly cyan = "#5bc0de";

  /**
   * called "Warning"
   */
  static readonly yellow = "#f0ad4e";

  /**
   * called "Danger"
   */
  static readonly red = "#d9534f";
}
