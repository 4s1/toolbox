/**
 * Color Palette
 *
 * Based on https://flatui.com/flat-ui-color-palette/
 */
export abstract class ColorPalette {
  /**
   * Turquoise
   */
  static readonly lightTurquoise = "#1abc9c";

  /**
   * Green Sea
   */
  static readonly darkTurquoise = "#16a085";

  /**
   * Emerald
   */
  static readonly lightGreen = "#2ecc71";

  /**
   * Nephritis
   */
  static readonly dargGreen = "#27ae60";

  /**
   * Peter River
   */
  static readonly lightBlue = "#3498db";

  /**
   * Belize Hole
   */
  static readonly darkBlue = "#2980b9";

  /**
   * Amethyst
   */
  static readonly lightPurple = "#9b59b6";

  /**
   * Wisteria
   */
  static readonly darkPurple = "#8e44ad";

  /**
   * Wet Asphalt
   */
  static readonly lightBlack = "#34495e";

  /**
   * Midnight Blue
   */
  static readonly darkBlack = "#2c3e50";

  /**
   * Sun Flower
   */
  static readonly lightYellow = "#f1c40f";

  /**
   * Orange
   */
  static readonly darkYellow = "#f39c12";

  /**
   * Carrot
   */
  static readonly lightOrange = "#e67e22";

  /**
   * Pumpkin
   */
  static readonly darkOrange = "#d35400";

  /**
   * Alizarin
   */
  static readonly lightRed = "#e74c3c";

  /**
   * Pomegranate
   */
  static readonly darkRed = "#c0392b";

  /**
   * Clouds
   */
  static readonly lightWhite = "#ecf0f1";

  /**
   * Silver
   */
  static readonly darkWhite = "#bdc3c7";

  /**
   * Concrete
   */
  static readonly lightGray = "#95a5a6";

  /**
   * Asbestos
   */
  static readonly darkGray = "#7f8c8d";
}
