/**
 * Bootstrap5 colors
 *
 * Based on https://getbootstrap.com/docs/5.1/customize/color/
 */
export abstract class Bootstrap5Color {
  static readonly blue = "#0d6efd";
  static readonly indigo = "#6610f2";
  static readonly purple = "#6f42c1";
  static readonly pink = "#d63384";
  static readonly red = "#dc3545";
  static readonly orange = "#fd7e14";
  static readonly yellow = "#ffc107";
  static readonly green = "#198754";
  static readonly teal = "#20c997";
  static readonly cyan = "#0dcaf0";
  static readonly gray = "#adb5bd";
  static readonly white = "#ffffff";
  static readonly black = "#000000";
}
