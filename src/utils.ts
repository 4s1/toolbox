import { AssertionError } from "./assertion-error.js";

/**
 * Check parameter to be unequals undefined and unequals null.
 */
export function isSet<T>(value: T | undefined | null): value is NonNullable<T> {
  return value !== undefined && value !== null;
}

/**
 * Throws an error of parameter is undefined or null.
 */
export function assertSet<T>(value: T | undefined | null): asserts value is NonNullable<T> {
  if (value === undefined || value === null) {
    throw new AssertionError(`Expected value to be defined, but received ${value}`);
  }
}

/**
 * Exhaustive guard for enums.
 *
 * @example
 * ```ts
 * enum SomeType {
 *   Foo,
 *   Bar,
 * }
 *
 * function getValue(some: SomeType): number {
 *   switch (some) {
 *     case SomeType.Foo:
 *       return 23;
 *     case SomeType.Bar:
 *       return 34;
 *     default:
 *       // We should never reach this default case
 *       exhaustiveGuard(some);
 *   }
 * }
 * ```
 * Source: https://meticulous.ai/blog/safer-exhaustive-switch-statements-in-typescript/
 */
export function exhaustiveGuard(_value: never): never {
  throw new AssertionError(
    `Reached forbidden guard function with unexpected value: ${JSON.stringify(_value)}`,
  );
}
