import { DefaultError } from "./default-error.js";

export class AssertionError extends DefaultError {}
