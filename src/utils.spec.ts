import { DefaultError } from "./default-error.js";
import { assertSet, isSet } from "./utils.js";
import { test } from "node:test";
import assert from "node:assert";

test("isSet() - Value is a string", (t) => {
  // Arrange
  const testVar = "foobar";
  // Act
  const res = isSet(testVar);
  // Assert
  assert.strictEqual(res, true);
});

test("isSet() - Value is 0", (t) => {
  // Arrange
  const testVar = 0;
  // Act
  const res = isSet(testVar);
  // Assert
  assert.strictEqual(res, true);
});

test("isSet() - Value is null", (t) => {
  // Arrange
  const testVar = null;
  // Act
  const res = isSet(testVar);
  // Assert
  assert.strictEqual(res, false);
});

test("isSet() - Value is undefined", (t) => {
  // Arrange
  const testVar = undefined;
  // Act
  const res = isSet(testVar);
  // Assert
  assert.strictEqual(res, false);
});

test("assertSet() - Value is a string", (t) => {
  // Arrange
  const testVar = "foobar";
  // Act
  const fct = (): void => {
    assertSet(testVar);
  };
  // Assert
  assert.doesNotThrow(fct);
});

test("assertSet() - Value is 0", (t) => {
  // Arrange
  const testVar = 0;
  // Act
  const fct = (): void => {
    assertSet(testVar);
  };
  // Assert
  assert.doesNotThrow(fct);
});

test("assertSet() - Value is null", (t) => {
  // Arrange
  const testVar = null;
  // Act
  const fct = (): void => {
    assertSet(testVar);
  };
  // Assert
  assert.throws(fct);
});

test("assertSet() - Value is undefined", (t) => {
  // Arrange
  const testVar = undefined;
  // Act
  const fct = (): void => {
    assertSet(testVar);
  };
  // Assert
  assert.throws(fct);
});
